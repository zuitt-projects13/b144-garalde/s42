// retrieve an element from the webpage

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// single quote only, # for id
// "document" refers to the whole webpage
// "querySelector" is used to select a specific object (HTML element) from the document


/*

Alternatively we can use getElement function to retrieve the elements.

document.getElementById('txt-first-name');
document.getElementByClassName('txt-input');
document.getElementByTag('input')


*/

// performs an action when an event triggers
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
	// console.log(event.target);
	// console.log(event.target.value)
})


txtFirstName.addEventListener('keyup', (event) => {
	// spanFullName.innerHTML = txtFirstName.value

	// event.target contains the element where the event happened 
	console.log(event.target);
	// event.target.value this will get the value of the input object
	console.log(event.target.value)
})


// performs an action when an event triggers
txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtLastName.value
	// console.log(event.target);
	// console.log(event.target.value)
})


txtLastName.addEventListener('keyup', (event) => {
	// spanFullName.innerHTML = txtFirstName.value

	// event.target contains the element where the event happened 
	console.log(event.target);
	// event.target.value this will get the value of the input object
	console.log(event.target.value)
})