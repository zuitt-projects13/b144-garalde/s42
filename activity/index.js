// retrieve an element from the webpage

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// single quote only, # for id
// "document" refers to the whole webpage
// "querySelector" is used to select a specific object (HTML element) from the document


/*

Alternatively we can use getElement function to retrieve the elements.

document.getElementById('txt-first-name');
document.getElementByClassName('txt-input');
document.getElementByTag('input')


*/


function fullname() {
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value
}


// performs an action when an event triggers
txtFirstName.addEventListener('keyup', (event) => {
	fullname()
	console.log(event.target);
	console.log(event.target.value)
})




txtLastName.addEventListener('keyup', (event) => {
	fullname()

	console.log(event.target);
	console.log(event.target.value)
})


